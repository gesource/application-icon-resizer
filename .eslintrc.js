module.exports = {
    "extends": "google",
    "installedESLint": true,
    "env": {
        // browser グローバル変数を使用する
        "browser": true,
    },
    "rules": {
        // インデントスタイルは4スペースに強制
        "indent": ["error", 4],
        // 改行コードはWindows
        "linebreak-style": ["error", "windows"],
        // alert関数を許可
        "no-alert": 0,
    },
};