'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var config = {
    iconSize: {
        windows: [16, // Explorer Details/List
        32, // Desktop Small symbols
        48, // Desktop Medium symbols
        256 // Desktop Large symbols
        ],
        osx: [16, 32, 128, 256, // OSX10.5+
        512, // OSX 10.5+
        1024 // OSX 10.7+
        ],
        android: [36, 48, 72, 96, 144],
        iphone: [57, 60, 87, 114, 120, 180, 29, 40, 58, 80],
        ipad: [72, 76, 144, 152, 40, 50, 80, 100, 29, 58]
    }
};

/**
 * {size:画像サイズ, blob:画像データ}の配列
 */
var blobs = [];

/**
 * エラーメッセージを表示する
 * @param {string} message エラーメッセージ
 */
function onError(message) {
    alert(message);
}

var PNGFile = function () {
    function PNGFile() {
        _classCallCheck(this, PNGFile);
    }

    _createClass(PNGFile, null, [{
        key: 'isPNFFile',


        /**
         * @param {File} file PNGファイル
         * @return {Boolean} fileがPNGファイルのときはtrue
         */
        value: function isPNFFile(file) {
            return file.type === 'image/png';
        }

        /**
         * PNGファイルを解析する
         * @param {File} file PNGファイル
         * @param {function} callback 引数にPNGファイルの解析結果をとるコールバック関数
         */

    }, {
        key: 'parseFile',
        value: function parseFile(file, callback) {
            var result = {};
            var fileReader = new FileReader();
            fileReader.onload = function (e) {
                var getBinValue = function getBinValue(bin, i, size) {
                    var v = 0;
                    for (var j = 0; j < size; j++) {
                        var b = bin.charCodeAt(i + j);
                        v = (v << 8) + b;
                    }
                    return v;
                };
                result.name = file.name;
                result.width = getBinValue(e.target.result, 8 + 0x08, 4);
                result.height = getBinValue(e.target.result, 8 + 0x0c, 4);
                result.size = file.size;
                result.lastModifiedDate = file.lastModifiedDate;

                var reader = new FileReader();
                reader.onload = function (e) {
                    result.src = reader.result;
                    callback(result);
                };
                reader.readAsDataURL(file);
            };
            fileReader.readAsBinaryString(file);
        }
    }]);

    return PNGFile;
}();

var ThumImage = function () {
    /**
     * @param {File} file 元画像
     * @param {int} size 元画像のサイズ
     */
    function ThumImage(file, size) {
        var _this = this;

        _classCallCheck(this, ThumImage);

        // this.file = file;
        this.size = size;
        this.img = new Image();
        this.img.width = size;
        this.img.height = size;
        this.img.name = 'thum-' + size;
        this.img.dataset.size = size;
        var reader = new FileReader();
        reader.onload = function (e) {
            _this.img.src = reader.result;
        };
        reader.readAsDataURL(file);
    }

    /**
     * サイズを変更した画像を作成する
     * @param {int} targetSizes 変換するサイズ
     */


    _createClass(ThumImage, [{
        key: 'resizeImages',
        value: function resizeImages(targetSizes) {
            blobs = [];
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = targetSizes[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var size = _step.value;

                    this._resizeImage(size);
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        }

        /**
         * @param {int} targetSize 変換するサイズ
         */

    }, {
        key: '_resizeImage',
        value: function _resizeImage(targetSize) {
            var canvas = document.createElement("canvas");
            canvas.width = targetSize;
            canvas.height = targetSize;
            var context = canvas.getContext('2d');
            context.drawImage(this.img, 0, 0, this.size, this.size, 0, 0, targetSize, targetSize);
            var img = new Image();
            img.width = targetSize;
            img.height = targetSize;
            img.src = canvas.toDataURL();
            img.dataset.size = targetSize;
            canvas.toBlob(function (blob) {
                blobs.push({ size: targetSize, blob: blob });
            }, 'image/png');
        }
    }]);

    return ThumImage;
}();

var ImageSource = function () {
    function ImageSource() {
        _classCallCheck(this, ImageSource);
    }

    _createClass(ImageSource, [{
        key: 'addFile',

        /**
         * アップロードされたファイルを登録します。
         * @param {FileList} files 登録するファイル
         * @param {function} onEnd 登録後実行されるコールバック関数
         */
        value: function addFile(files, onEnd) {
            var _this2 = this;

            if (files.length === 0) {
                onEnd();
                return;
            }
            this.file = files[0];
            PNGFile.parseFile(files[0], function (png) {
                _this2.png = png;
                _this2.thum = new ThumImage(_this2.file, _this2.png.width);
                onEnd(png);
            });
        }

        /**
         * サムネイル画像を作成する
         * @param {Array} sizes 作成する画像サイズの配列
         */

    }, {
        key: 'resizeImages',
        value: function resizeImages(sizes) {
            this.thum.resizeImages(sizes);
        }

        /**
         * サムネイル画像を格納したZIPを作成する
         * @param {Function} callback 作成したZIPのblobURL
         */

    }, {
        key: 'createZip',
        value: function createZip(callback) {
            var _this3 = this;

            this.writer = new zip.BlobWriter();
            zip.createWriter(this.writer, function (writer) {
                _this3.zipWriter = writer;
                _this3._addZip(function () {
                    _this3.zipWriter.close(function (blob) {
                        var blobURL = window.URL.createObjectURL(blob);
                        callback(blobURL);
                        _this3.zipWriter = null;
                        _this3.write = null;
                    });
                });
            }, onError);
        }

        /**
         * @param {Function} onEnd すべての画像を追加した後に呼ばれるコールバック関数
         */

    }, {
        key: '_addZip',
        value: function _addZip(onEnd) {
            var _this4 = this;

            if (blobs.length === 0) {
                onEnd();
                return;
            }
            var image = blobs.shift();
            var filename = image.size + '.png';
            var reader = new zip.BlobReader(image.blob);
            this.zipWriter.add(filename, reader,
            // new zip.BlobReader(image.blog),
            function () {
                _this4._addZip(onEnd);
            });
        }

        /**
         * サムネイル画像を作成する
         * @param {Array} sizeList サムネイル画像のファイルサイズ
         */

    }, {
        key: 'createThumb',
        value: function createThumb(sizeList) {
            var _this5 = this;

            var _loop = function _loop(i) {
                var thum = new ThumImage(_this5.file, sizeList[i]);
                thum.createBlob(function (blob) {
                    _this5.addZip(sizeList[i] + '.png', blob);
                });
            };

            for (var i = 0; i < sizeList.length; i++) {
                _loop(i);
            }
        }
    }]);

    return ImageSource;
}();

function init() {
    var imageSrc = new ImageSource();
    var fileInput = document.getElementById("file-selector");
    var dragArea = document.getElementById('dragArea');
    var createIconButton = document.getElementById('create-icon-button');
    var downloadButton = document.getElementById('download-button');

    /**
     * 画像ファイルを登録する
     * @param {Array} files アップされた画像ファイル
     */
    var addFiles = function addFiles(files) {
        fileInput.disabled = true;
        dragArea.disabled = true;
        createIconButton.style = 'display:none';
        imageSrc.addFile(files, function (png) {
            document.getElementById('image-info').innerHTML = 'File name: ' + png.name + '<br>\n                File size： ' + png.size + ' byte<br>\n                Last modified date: ' + png.lastModifiedDate + '<br>\n                Image width: ' + png.width + ' px<br>\n                Image height: ' + png.height + ' px';

            var img = document.getElementById("image-thumb");
            img.width = 64;
            img.height = 64;
            img.src = png.src;

            fileInput.disabled = false;
            dragArea.disabled = false;
            createIconButton.style = 'display:block';
        });
    };

    fileInput.addEventListener('change', function () {
        addFiles(fileInput.files);
    }, false);

    dragArea.addEventListener('dragover', function (e) {
        e.stopPropagation();
        e.preventDefault();
        e.dataTransfer.dropEffect = 'copy';
    }, false);
    dragArea.addEventListener('drop', function (e) {
        e.stopPropagation();
        e.preventDefault();
        addFiles(e.dataTransfer.files);
    }, false);

    /**
     * 作成するアイコンのサイズを取得する
     * @return {Array} アイコンサイズの配列
     */
    var getIconSize = function getIconSize() {
        var form = document.getElementById('form');
        var radioNodeList = form.os;
        return config.iconSize[radioNodeList.value];
    };

    createIconButton.addEventListener('click', function (event) {
        imageSrc.resizeImages(getIconSize());
        imageSrc.createZip(function (blobURL) {
            if (blobURL) {
                downloadButton.href = blobURL;
                downloadButton.download = 'images.zip';
                downloadButton.style = 'display: block';
            } else {
                downloadButton.style = 'display: none';
            }
        });
        event.preventDefault();
        return false;
    });

    var radiobuttons = document.getElementById('form').os;
    var _iteratorNormalCompletion2 = true;
    var _didIteratorError2 = false;
    var _iteratorError2 = undefined;

    try {
        for (var _iterator2 = radiobuttons[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
            var radio = _step2.value;

            radio.addEventListener('click', function () {
                downloadButton.style = 'display: none';
            }, false);
        }
    } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
    } finally {
        try {
            if (!_iteratorNormalCompletion2 && _iterator2.return) {
                _iterator2.return();
            }
        } finally {
            if (_didIteratorError2) {
                throw _iteratorError2;
            }
        }
    }
}

window.addEventListener('load', init);