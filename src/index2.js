'use strict';


/**
 * エラーメッセージを表示する
 * @param {string} message エラーメッセージ
 */
function onError(message) {
    alert(message);
}

class Model {
    /**
     * @param {FileList} files 登録するファイル
     * @param {Function} onEnd 終了後のコールバック
     */
    addFiles(files, onEnd) {
        if (files.length === 0) {
            return;
        }
        const file = files[0];
        this.addFile(file, onEnd);
    }

    /**
     * @param {File} file 登録するファイル
     * @param {Function} onEnd 終了後のコールバック
     */
    addFile(file, onEnd) {
        try {
            if (file.type !== 'image/png') {
                onError('PNGファイルを選択してください。');
                return;
            }
            this.writer = new zip.BlobWriter();
            zip.createWriter(this.writer, (writer) => {
                this.zipWriter = writer;
                this.add(file);
            }, onError);
        } catch (e) {
            onError(e.message);
        } finally {
            onEnd();
        }
    }

    /**
     * @param {File} file 登録するファイル
     */
    add(file) {
        this.zipWriter.add(file.name, new zip.BlobReader(file), function() {
        });
    }

    getBlobURL(callback) {
        // this.zipWriter.close(function(blob) {
        this.zipWriter.close(blob => {
            const blobURL = window.URL.createObjectURL(blob);
            callback(blobURL);
            // this.zipWriter = null;
        });
    }
}

function init() {
    const model = new Model();

    const fileInput = document.getElementById("file-input");
    fileInput.addEventListener('change', function() {
        fileInput.disabled = true;
        model.addFiles(
            fileInput.files,
            // onend
            function() {
                fileInput.disabled = false;
            });
    }, false);
    const createIconButton = document.getElementById('create-icon-button');
    const downloadButton = document.getElementById('download-button');
    createIconButton.addEventListener('click', function(event) {
        model.getBlobURL(function(blobURL) {
            downloadButton.href = blobURL;
            downloadButton.download = 'test.zip';
            // const clickEvent = document.createEvent("MouseEvent");
            // clickEvent.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
            // downloadButton.dispatchEvent(clickEvent);
        });
        event.preventDefault();
        return false;
    });
    const createTargetOS = function(element) {
        const iconSize = {
            windows: [
                16, // Explorer Details/List
                32, // Desktop Small symbols
                48, // Desktop Medium symbols
                256 // Desktop Large symbols
            ],
            osx: [
                16,
                32,
                128,
                256, // OSX10.5+
                512, // OSX 10.5+
                1024 // OSX 10.7+
            ],
            android: [36, 48, 72, 96, 144],
            iphone: [57, 60, 87, 114, 120, 180, 29, 40, 58, 80],
            ipad: [72, 76, 144, 152, 40, 50, 80, 100, 29, 58]
        };
    };
    createTargetOS(document.getElementById('target-os'));
}

window.addEventListener('load', init);
