"use strict";

class PNGFile {

    /**
     * @param {File} file PNGファイル
     * @return {Boolean} fileがPNGファイルのときはtrue
     */
    static isPNFFile(file) {
        return (file.type === 'image/png');
    }

    /**
     * PNGファイルを解析する
     * @param {File} file PNGファイル
     * @param {function} callback 引数にPNGファイルの解析結果をとるコールバック関数
     */
    static parseFile(file, callback) {
        const result = {};
        const fileReader = new FileReader();
        fileReader.onload = function(e) {
            const getBinValue = function(bin, i, size) {
                var v = 0;
                for (let j = 0; j < size; j++) {
                    const b = bin.charCodeAt(i + j);
                    v = (v << 8) + b;
                }
                return v;
            };
            result.name = file.name;
            result.width = getBinValue(e.target.result, 8 + 0x08, 4);
            result.height = getBinValue(e.target.result, 8 + 0x0c, 4);
            result.size = file.size;
            result.lastModifiedDate = file.lastModifiedDate;

            const reader = new FileReader();
            reader.onload = function(e) {
                result.src = reader.result;
                callback(result);
            };
            reader.readAsDataURL(file);
        };
        fileReader.readAsBinaryString(file);
    }
}

/**
 * 初期化処理
 */
function init() {
    /**
     * PNGファイルを解析し、表示します。
     * @param {File} file PNGファイル
     */
    const parsePNGFile = function(file) {
        if (!PNGFile.isPNFFile(file)) {
            alert('pngファイルを選んでください。');
            return;
        }

        PNGFile.parseFile(file, function(png) {
            document.getElementById('info').innerHTML =
                `ファイル名:${png.name}<br>
ファイルサイズ：${png.size}<br>
最終更新日時:${png.lastModifiedDate}<br>
幅:${png.width}px<br>
高さ:${png.height}px`;

            const img = document.getElementById("image-original");
            img.width = png.width;
            img.height = png.height;
            img.src = png.src;
        });
    };

    /**
     * ファイルがアップロードされたときのイベント
     * @param {Event} e イベント
     */
    const fileSelect = function(e) {
        if (e.target.files.length === 0) {
            return;
        }
        const file = e.target.files[0];
        parsePNGFile(file);
    };
    const fileSelector = document.getElementById('file-selector');
    fileSelector.addEventListener('change', fileSelect, false);

    const imageUpload = document.getElementById('imageupload');
    imageUpload.addEventListener('click', function(e) {
        fileSelector.click();
    }, false);

    const dragOver = function(e) {
        e.stopPropagation();
        e.preventDefault();
        e.dataTransfer.dropEffect = 'copy';
    };

    const dropFile = function(e) {
        e.stopPropagation();
        e.preventDefault();

        if (e.dataTransfer.files === 0) {
            return;
        }
        const file = e.dataTransfer.files[0];
        parsePNGFile(file);
    };

    const dragArea = document.getElementById('dragArea');
    dragArea.addEventListener('dragover', dragOver, false);
    dragArea.addEventListener('drop', dropFile, false);
}

window.onload = function() {
    init();
};
