'use strict';

var blobs = {};

/**
 * エラーメッセージを表示する
 * @param {string} message エラーメッセージ
 */
function onError(message) {
    alert(message);
}

class PNGFile {

    /**
     * @param {File} file PNGファイル
     * @return {Boolean} fileがPNGファイルのときはtrue
     */
    static isPNFFile(file) {
        return (file.type === 'image/png');
    }

    /**
     * PNGファイルを解析する
     * @param {File} file PNGファイル
     * @param {function} callback 引数にPNGファイルの解析結果をとるコールバック関数
     */
    static parseFile(file, callback) {
        const result = {};
        const fileReader = new FileReader();
        fileReader.onload = function(e) {
            const getBinValue = function(bin, i, size) {
                var v = 0;
                for (let j = 0; j < size; j++) {
                    const b = bin.charCodeAt(i + j);
                    v = (v << 8) + b;
                }
                return v;
            };
            result.name = file.name;
            result.width = getBinValue(e.target.result, 8 + 0x08, 4);
            result.height = getBinValue(e.target.result, 8 + 0x0c, 4);
            result.size = file.size;
            result.lastModifiedDate = file.lastModifiedDate;

            const reader = new FileReader();
            reader.onload = function(e) {
                result.src = reader.result;
                callback(result);
            };
            reader.readAsDataURL(file);
        };
        fileReader.readAsBinaryString(file);
    }
}

class ThumImage {
    /**
     * @param {File} file 元画像
     * @param {int} size 元画像のサイズ
     */
    constructor(file, size) {
        // this.file = file;
        this.size = size;
        this.img = new Image();
        this.img.width = size;
        this.img.height = size;
        this.img.name = `thum-${size}`;
        this.img.dataset.size = size;
        const reader = new FileReader();
        reader.onload = e => {
            this.img.src = reader.result;
        };
        reader.readAsDataURL(file);
    }

    resizeImages(targetSizes, parent) {
        parent.textContent = null;
        for (let size of targetSizes) {
            this.resizeImage(size, parent);
        }
    }

    /**
     * @param {int} targetSize 変換するサイズ
     * @param {element} parent 変換した画像を表示するimgタグの親エレメント
     */
    resizeImage(targetSize, parent) {
        const canvas = document.createElement("canvas");
        canvas.width = targetSize;
        canvas.height = targetSize;
        const context = canvas.getContext('2d');
        context.drawImage(
            this.img,
            0,
            0,
            this.size,
            this.size,
            0,
            0,
            targetSize,
            targetSize);
        const img = new Image();
        img.width = targetSize;
        img.height = targetSize;
        img.src = canvas.toDataURL();
        img.dataset.size = targetSize;
        canvas.toBlob(blob => {
            blobs[targetSize] = blob;
            parent.appendChild(img);
        }, 'image/png');
    }
}

class ImageSource {
    /**
     * @param {FileList} files 登録するファイル
     */
    addFile(files) {
        if (files.length === 0) {
            return;
        }
        this.file = files[0];
        PNGFile.parseFile(files[0], png => {
            this.png = png;
            this.thum = new ThumImage(this.file, this.png.width);
        });
    }

    /**
     * サムネイル画像を作成する
     * @param {Array} sizes 作成する画像サイズの配列
     * @param {element} parent 作成した画像を表示する親エレメント
     */
    resizeImages(sizes, parent) {
        this.thum.resizeImages(sizes, parent);
    }

    /**
     * サムネイル画像を格納したZIPを作成する
     * @param {HTMLElement} parent このエレメントの下の画像を格納する
     * @param {Function} callback 作成したZIPのblobURL
     */
    createZip(parent, callback) {
        this.writer = new zip.BlobWriter();
        zip.createWriter(this.writer, writer => {
            this.zipWriter = writer;
            const images = parent.children;
            this.addZip(
                images,
                0,
                () => {
                    this.zipWriter.close(blob => {
                        const blobURL = window.URL.createObjectURL(blob);
                        callback(blobURL);
                        this.zipWriter = null;
                        this.write = null;
                    });
                });
        }, onError);
    }

    /**
     * @param {HTMLCollection} images imageタグの一覧
     * @param {int} index 処理するimageタグのインデックス
     */
    addZip(images, index, onEnd) {
        const img = images.item(index);
        if (!img) {
            onEnd();
            return;
        }
        this.zipWriter.add(
            `${img.dataset.size}.png`,
            new zip.BlobReader(blobs[img.dataset.size]),
            () => {
                this.addZip(images, index + 1, onEnd);
            }
        );
    }

    /**
     * サムネイル画像を作成する
     * @param {Array} sizeList サムネイル画像のファイルサイズ
     */
    createThumb(sizeList, parent) {
        for (let i = 0; i < sizeList.length; i++) {
            const thum = new ThumImage(this.file, sizeList[i]);
            thum.createBlob(blob => {
                this.addZip(`${sizeList[i]}.png`, blob);
            });
        }
    }
/*
    addZip(filename, blob) {
        this.zipWriter.add(
            filename,
            new zip.BlobReader(blob),
            function() {
            }
        );
    }
*/
}

class Model {
    /**
     * @param {FileList} files 登録するファイル
     * @param {Function} onEnd 終了後のコールバック
     */
    addFiles(files, onEnd) {
        if (files.length === 0) {
            return;
        }
        const file = files[0];
        this.addFile(file, onEnd);
    }

    /**
     * @param {File} file 登録するファイル
     * @param {Function} onEnd 終了後のコールバック
     */
    addFile(file, onEnd) {
        try {
            if (file.type !== 'image/png') {
                onError('PNGファイルを選択してください。');
                return;
            }
            if (this.zipWriter === undefined) {
                this.writer = new zip.BlobWriter();
                zip.createWriter(this.writer, writer => {
                    this.zipWriter = writer;
                    this.add(file);
                }, onError);
            } else {
                this.add(file);
            }
        } catch (e) {
            onError(e.message);
        } finally {
            onEnd();
        }
    }

    add(file) {
        this.zipWriter.add(
            file.name,
            new zip.BlobReader(file),
            function() {
            }
        );
    }

    getBlobURL(callback) {
        if (this.zipWriter) {
            // this.zipWriter.close(function(blob) {
            this.zipWriter.close(blob => {
                const blobURL = window.URL.createObjectURL(blob);
                callback(blobURL);
                // this.zipWriter = null;
            });
        } else {
            callback(null);
        }
    }
}

function init() {
    const model = new Model();
    const imageSrc = new ImageSource();

    const fileInput = document.getElementById("file-selector");
    fileInput.addEventListener('change', function() {
        imageSrc.addFile(fileInput.files);
        /*
        fileInput.disabled = true;
        model.addFiles(
            fileInput.files,
            // onend
            function() {
                fileInput.disabled = false;
            });
        */
    }, false);

    const createThumbButton = document.getElementById('create-thumb-image');
    createThumbButton.addEventListener('click', function() {
        imageSrc.resizeImages(
            [16, 32, 64],
            document.getElementById('image-thumb'));
    }, false);

    const createIconButton = document.getElementById('create-icon-button');
    const downloadButton = document.getElementById('download-button');
    createIconButton.addEventListener('click', function(event) {
        imageSrc.createZip(
            document.getElementById('image-thumb'),
            function(blobURL) {
                if (blobURL) {
                    downloadButton.href = blobURL;
                    downloadButton.download = 'images.zip';
                    downloadButton.style = 'display: block';
                } else {
                    downloadButton.style = 'display: none';
                }
            }
        );
        /*
        model.getBlobURL(function(blobURL) {
            if (blobURL) {
                downloadButton.href = blobURL;
                downloadButton.download = 'test.zip';
                downloadButton.style = 'display: block';
            } else {
                downloadButton.style = 'display: none';
            }
            // const clickEvent = document.createEvent("MouseEvent");
            // clickEvent.initMouseEvent("click", true, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
            // downloadButton.dispatchEvent(clickEvent);
        });
        */
        event.preventDefault();
        return false;
    });
    const createTargetOS = function(element) {
        const iconSize = {
            windows: [
                16, // Explorer Details/List
                32, // Desktop Small symbols
                48, // Desktop Medium symbols
                256 // Desktop Large symbols
            ],
            osx: [
                16,
                32,
                128,
                256, // OSX10.5+
                512, // OSX 10.5+
                1024 // OSX 10.7+
            ],
            android: [36, 48, 72, 96, 144],
            iphone: [57, 60, 87, 114, 120, 180, 29, 40, 58, 80],
            ipad: [72, 76, 144, 152, 40, 50, 80, 100, 29, 58]
        };
    };
    createTargetOS(document.getElementById('target-os'));
}

window.addEventListener('load', init);
