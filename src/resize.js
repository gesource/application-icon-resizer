"use strict";

class PNGFile {

    /**
     * @param {File} file PNGファイル
     * @return {Boolean} fileがPNGファイルのときはtrue
     */
    static isPNFFile(file) {
        return (file.type === 'image/png');
    }

    /**
     * PNGファイルを解析する
     * @param {File} file PNGファイル
     * @param {function} callback 引数にPNGファイルの解析結果をとるコールバック関数
     */
    static parseFile(file, callback) {
        const result = {};
        const fileReader = new FileReader();
        fileReader.onload = function(e) {
            const getBinValue = function(bin, i, size) {
                var v = 0;
                for (let j = 0; j < size; j++) {
                    const b = bin.charCodeAt(i + j);
                    v = (v << 8) + b;
                }
                return v;
            };
            result.name = file.name;
            result.width = getBinValue(e.target.result, 8 + 0x08, 4);
            result.height = getBinValue(e.target.result, 8 + 0x0c, 4);
            result.size = file.size;
            result.lastModifiedDate = file.lastModifiedDate;

            const reader = new FileReader();
            reader.onload = function(e) {
                result.src = reader.result;
                callback(result);
            };
            reader.readAsDataURL(file);
        };
        fileReader.readAsBinaryString(file);
    }
}

/**
 * 初期化処理
 */
function init() {
    /**
     * @param {IMG} img 変換元の画像のimgタグ
     * @param {int} width 変換する幅
     * @param {int} height 変換する高さ
     * @param {IMG} target 変換した画像を表示するimgタグ
     */
    const resizeImage = function(img, width, height, target) {
        const canvas = document.createElement("canvas");
        canvas.width = width;
        canvas.height = height;
        const context = canvas.getContext('2d');
        context.drawImage(img, 0, 0, img.width, img.height, 0, 0, width, height);
        target.width = width;
        target.height = height;
        target.src = canvas.toDataURL();
    };
    document.getElementById('resizeImage').addEventListener('click',
        function() {
            resizeImage(
                document.getElementById('image-original'),
                32,
                32,
                document.getElementById('image-thumb')
            );
        }, false);
}

window.onload = function() {
    init();
};
