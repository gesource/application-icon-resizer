'use strict';

const config = {
    iconSize: {
        windows: [
            16, // Explorer Details/List
            32, // Desktop Small symbols
            48, // Desktop Medium symbols
            256 // Desktop Large symbols
        ],
        osx: [
            16,
            32,
            128,
            256, // OSX10.5+
            512, // OSX 10.5+
            1024 // OSX 10.7+
        ],
        android: [36, 48, 72, 96, 144],
        iphone: [57, 60, 87, 114, 120, 180, 29, 40, 58, 80],
        ipad: [72, 76, 144, 152, 40, 50, 80, 100, 29, 58]
    }
};

/**
 * {size:画像サイズ, blob:画像データ}の配列
 */
var blobs = [];

/**
 * エラーメッセージを表示する
 * @param {string} message エラーメッセージ
 */
function onError(message) {
    alert(message);
}

class PNGFile {

    /**
     * @param {File} file PNGファイル
     * @return {Boolean} fileがPNGファイルのときはtrue
     */
    static isPNFFile(file) {
        return (file.type === 'image/png');
    }

    /**
     * PNGファイルを解析する
     * @param {File} file PNGファイル
     * @param {function} callback 引数にPNGファイルの解析結果をとるコールバック関数
     */
    static parseFile(file, callback) {
        const result = {};
        const fileReader = new FileReader();
        fileReader.onload = function(e) {
            const getBinValue = function(bin, i, size) {
                var v = 0;
                for (let j = 0; j < size; j++) {
                    const b = bin.charCodeAt(i + j);
                    v = (v << 8) + b;
                }
                return v;
            };
            result.name = file.name;
            result.width = getBinValue(e.target.result, 8 + 0x08, 4);
            result.height = getBinValue(e.target.result, 8 + 0x0c, 4);
            result.size = file.size;
            result.lastModifiedDate = file.lastModifiedDate;

            const reader = new FileReader();
            reader.onload = function(e) {
                result.src = reader.result;
                callback(result);
            };
            reader.readAsDataURL(file);
        };
        fileReader.readAsBinaryString(file);
    }
}

class ThumImage {
    /**
     * @param {File} file 元画像
     * @param {int} size 元画像のサイズ
     */
    constructor(file, size) {
        // this.file = file;
        this.size = size;
        this.img = new Image();
        this.img.width = size;
        this.img.height = size;
        this.img.name = `thum-${size}`;
        this.img.dataset.size = size;
        const reader = new FileReader();
        reader.onload = e => {
            this.img.src = reader.result;
        };
        reader.readAsDataURL(file);
    }

    /**
     * サイズを変更した画像を作成する
     * @param {int} targetSizes 変換するサイズ
     */
    resizeImages(targetSizes) {
        blobs = [];
        for (let size of targetSizes) {
            this._resizeImage(size);
        }
    }

    /**
     * @param {int} targetSize 変換するサイズ
     */
    _resizeImage(targetSize) {
        const canvas = document.createElement("canvas");
        canvas.width = targetSize;
        canvas.height = targetSize;
        const context = canvas.getContext('2d');
        context.drawImage(
            this.img,
            0,
            0,
            this.size,
            this.size,
            0,
            0,
            targetSize,
            targetSize);
        const img = new Image();
        img.width = targetSize;
        img.height = targetSize;
        img.src = canvas.toDataURL();
        img.dataset.size = targetSize;
        canvas.toBlob(blob => {
            blobs.push({size: targetSize, blob: blob});
        }, 'image/png');
    }
}

class ImageSource {
    /**
     * @param {FileList} files 登録するファイル
     * @param {function} onEnd 登録後実行されるコールバック関数
     */
    addFile(files, onEnd) {
        if (files.length === 0) {
            onEnd();
            return;
        }
        this.file = files[0];
        PNGFile.parseFile(files[0], png => {
            this.png = png;
            this.thum = new ThumImage(this.file, this.png.width);
            onEnd();
        });
    }

    /**
     * サムネイル画像を作成する
     * @param {Array} sizes 作成する画像サイズの配列
     */
    resizeImages(sizes) {
        this.thum.resizeImages(sizes);
    }

    /**
     * サムネイル画像を格納したZIPを作成する
     * @param {Function} callback 作成したZIPのblobURL
     */
    createZip(callback) {
        this.writer = new zip.BlobWriter();
        zip.createWriter(this.writer, writer => {
            this.zipWriter = writer;
            this._addZip(
                () => {
                    this.zipWriter.close(blob => {
                        const blobURL = window.URL.createObjectURL(blob);
                        callback(blobURL);
                        this.zipWriter = null;
                        this.write = null;
                    });
                });
        }, onError);
    }

    /**
     * @param {Function} onEnd すべての画像を追加した後に呼ばれるコールバック関数
     */
    _addZip(onEnd) {
        if (blobs.length === 0) {
            onEnd();
            return;
        }
        const image = blobs.shift();
        console.log(image);
        const filename = `${image.size}.png`;
        console.log(filename);
        const reader = new zip.BlobReader(image.blob);
        this.zipWriter.add(
            filename,
            reader,
            // new zip.BlobReader(image.blog),
            () => {
                this._addZip(onEnd);
            }
        );
    }

    /**
     * サムネイル画像を作成する
     * @param {Array} sizeList サムネイル画像のファイルサイズ
     */
    createThumb(sizeList) {
        for (let i = 0; i < sizeList.length; i++) {
            const thum = new ThumImage(this.file, sizeList[i]);
            thum.createBlob(blob => {
                this.addZip(`${sizeList[i]}.png`, blob);
            });
        }
    }
}

function init() {
    const imageSrc = new ImageSource();

    const fileInput = document.getElementById("file-selector");
    fileInput.addEventListener('change', function() {
        fileInput.disabled = true;
        imageSrc.addFile(fileInput.files, function() {
            console.log('fileInput.onEnd');
            fileInput.disabled = false;
        });
    }, false);

    /**
     * 作成するアイコンのサイズを取得する
     * @return {Array} アイコンサイズの配列
     */
    const getIconSize = function() {
        const form = document.getElementById('form');
        console.log(form);
        const radioNodeList = form.os;
        console.log(radioNodeList);
        console.log(radioNodeList.value);
        return config.iconSize[radioNodeList.value];
    };

    const createIconButton = document.getElementById('create-icon-button');
    const downloadButton = document.getElementById('download-button');
    createIconButton.addEventListener('click', function(event) {
        imageSrc.resizeImages(getIconSize());
        imageSrc.createZip(
            function(blobURL) {
                if (blobURL) {
                    downloadButton.href = blobURL;
                    downloadButton.download = 'images.zip';
                    downloadButton.style = 'display: block';
                } else {
                    downloadButton.style = 'display: none';
                }
            }
        );
        event.preventDefault();
        return false;
    });
}

window.addEventListener('load', init);
